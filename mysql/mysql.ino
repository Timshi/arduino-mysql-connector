#include <ESP8266WiFi.h>
#include <MySQL_Connection.h>
#include <MySQL_Cursor.h>

// database setup
IPAddress sqlIpAddress(, , , );
char sqlUser[] = "";
char sqlPassword[] = "";

// WiFi setup
char ssid[] = "";
char wifiPassword[] = "";

WiFiClient client;
MySQL_Connection conn(&client);
MySQL_Cursor* cursor;

void setup() {
  Serial.begin(9600);

  // connect to WiFi
  Serial.print("[DEBUG] Connecting to " + String(ssid));
  WiFi.begin(ssid, wifiPassword);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.print("[DEBUG] Connected to network with IP address ");
  Serial.println(WiFi.localIP());

  // connect to MySQL
  Serial.print("[DEBUG] Connecting to MySQL... ");
  String state = conn.connect(sqlIpAddress, 3306, sqlUser, sqlPassword) ?
                 "Connected" :
                 "FAILED!";
  Serial.println(state);
  
  cursor = new MySQL_Cursor(&conn);
}

void loop() {
  if (conn.connected()) {
    String query = "";
    Serial.println("[DEBUG] Query to execute: " + String(query));
    cursor -> execute(query.c_str());
  }
  
  delay(5000);
}